import { v4 } from 'uuid'

type UUID = string & {
  __brand: 'UUID.V4',
}

const uuid = (): UUID => v4() as any

export { UUID, uuid }
