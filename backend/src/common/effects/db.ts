import * as torm from 'typeorm'

type ConnectionOptions = {
  type: string
  host: string
  port: number
  username: string
  password: string
  database: string

  synchronize?: boolean
  logging?: boolean
  entities?: string[],
}

const connectToDatabase =
  async (opts: ConnectionOptions, tries: number = 0): Promise<torm.EntityManager> => {
    try {
      const { manager } = await torm.createConnection(opts as torm.ConnectionOptions)
      return manager
    } catch (err) {
      if (tries >= 10) {
        throw err
      }
      return new Promise((resolve, _) => {
        setTimeout(() => resolve(connectToDatabase(opts, tries + 1)), 1000)
      })
    }
  }

export { ConnectionOptions, connectToDatabase }
