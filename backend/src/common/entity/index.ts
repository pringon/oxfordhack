import { EntityManager } from 'typeorm'
import * as g from './Game'
import * as u from './User'

type DBHelpers = {
  game: g.GameHelpers
  user: u.UserHelpers,
}

const initHelpers = (c: EntityManager): DBHelpers => {
  return {
    game: g.initHelpers(c),
    user: u.initHelpers(c),
  }
}

export {
  g,
  u,
  DBHelpers,
  initHelpers,
}
