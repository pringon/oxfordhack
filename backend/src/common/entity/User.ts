import { Entity, PrimaryColumn, Column, EntityManager } from 'typeorm';
import { UUID, uuid } from '../util/uuid';

type UserNewOptions = {
  id?: UUID;
  gameId?: UUID;
  username? : string;
};

@Entity()
class User {
  @PrimaryColumn()
  id: UUID;

  @Column()
  username: string

  @Column()
  gameId: UUID;

  @Column('decimal')
  positionX: number;

  @Column('decimal')
  positionY: number;

  @Column('boolean')
  updateSent: boolean

}

type UserHelpers = {
  make: (opts: UserNewOptions) => User;
  put: (s: User) => Promise<void>;
  get: () => Promise<User[]>;
  getById: (id: UUID) => Promise<User>;
  getByGameId: (gameId: UUID) => Promise<User[]>;
};

const initHelpers = (c: EntityManager): UserHelpers => {
  return {
    make,
    put: put(c),
    get: get(c),
    getById: getById(c),
    getByGameId: getByGameId(c),
  };
};

const make = ({ id, gameId, username }: UserNewOptions): User => {
  const user = new User();
  user.id = id ?? uuid();
  user.username = username
  user.gameId = gameId
  user.positionX = Math.random() * 500;
  user.positionY = Math.random() * 500;
  user.updateSent = true;
  return user;
};

const put = (c: EntityManager) => async (m: User): Promise<void> => {
  await c.save(m);
  return;
};

const get = (c: EntityManager) => (): Promise<User[]> => c.find(User, {});

const getById = (c: EntityManager) => (id: UUID): Promise<User> =>
  c.findOne(User, id as string);

const getByGameId = (c: EntityManager) => (gameId: UUID): Promise<User[]> =>
  c.find(User, { where: { gameId } })

export default User;
export { UserNewOptions, User, UserHelpers, initHelpers };
