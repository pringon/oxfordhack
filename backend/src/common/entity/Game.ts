import { Entity, PrimaryColumn, Column, EntityManager } from 'typeorm';
import { UUID, uuid } from '../util/uuid';
import { User } from './User'

type GameNewOptions = {
  id?: UUID;
};

@Entity()
class Game {
  @PrimaryColumn()
  id: UUID;

  userList?: User[];

}

type GameHelpers = {
  make: (opts: GameNewOptions) => Game;
  put: (s: Game) => Promise<void>;
  get: () => Promise<Game[]>;
  getById: (id: UUID) => Promise<Game>;
};

const initHelpers = (c: EntityManager): GameHelpers => {
  return {
    make,
    put: put(c),
    get: get(c),
    getById: getById(c),
  };
};

const make = ({ id }: GameNewOptions): Game => {
  const game = new Game();

  game.id = id ?? uuid();

  return game;
};

const put = (c: EntityManager) => async (m: Game): Promise<void> => {
  await c.save(m);
  return;
};

const get = (c: EntityManager) => (): Promise<Game[]> => c.find(Game, {});

const getById = (c: EntityManager) => (id: UUID): Promise<Game> =>
  c.findOne(Game, id as string);

export default Game;
export { GameNewOptions, Game, initHelpers, GameHelpers };
