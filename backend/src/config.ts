import { asString, asNumber } from './common/util/environment'

type Config = {
  database: {
    type: 'postgres'
    host: string
    port: number
    username: string
    password: string,
    database: string,
  },
}

export {
  Config,
  makeConfig,
}

function makeConfig(): Config {
  return {
    database: {
      type: 'postgres',
      host: asString('DB_HOST', 'localhost'),
      port: asNumber('DB_PORT', 5432),
      username: asString('DB_USER', 'postgres'),
      password: asString('DB_PASSWORD', 'postgres'),
      database: asString('DB_NAME', 'metrics'),
    },
  }
}
