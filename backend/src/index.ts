import 'reflect-metadata'

import { makeConfig } from './config'
import { makeServer } from './socket/index'

async function main() {
  const config = makeConfig()

  const socketServer = await makeServer(config);

  socketServer.listen(8080)
}

main()
