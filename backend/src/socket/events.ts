import { UUID } from '../common/util/uuid'

export type SocketEventRequest = NewPlayerRequest | RefreshPlayerRequest | MovementRequest

export type SocketEventResponse = NewPlayerResponse | RefreshPlayerResponse | MovementResponse

type Coordinate = {
  x: number,
  y: number,
}

export type NewPlayerRequest = {
  command: 'NEW_PLAYER'
  gameId?: UUID,
  username: string,
}

export type NewPlayerResponse = {
  command: 'NEW_PLAYER'
  gameId: UUID,
  playerId: UUID,
  username: string,
  position : Coordinate,
}

export type RefreshPlayerRequest = {
  command: 'REFRESH_PLAYERS',
  playerId: UUID,
  gameId: UUID,
  position : Coordinate,
}

export type RefreshPlayerResponse = {
  command: 'REFRESH_PLAYERS',
  playerId: UUID,
  gameId: UUID,
  username: string,
  position : Coordinate,
}

export type MovementRequest = {
  command: 'MOVEMENT',
  playerId: UUID,
  gameId: UUID,
  position: Coordinate,
}

export type MovementResponse = {
  command: 'MOVEMENT',
  playerId: UUID,
  gameId: UUID,
  username: string,
  position: Coordinate,
}
