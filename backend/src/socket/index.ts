import * as WebSocket from 'ws'

import { Config } from '../config'
import { DBHelpers, initHelpers } from '../common/entity/index'
import { connectToDatabase } from '../common/effects/db'
import { UUID } from '../common/util/uuid'
import {
  MovementResponse,
  SocketEventResponse,
} from './events'
import {
  handleNewPlayer,
  handleRefreshPlayer,
  handleMovement,
} from './handlers'

type SocketServer = {
  listen: (port: number) => void,
}

type GameSocket = WebSocket & {
  id?: UUID;
  gameId?: UUID;
}

const makeServer = async (conf: Config): Promise<SocketServer> => {
  const entityManager = await connectToDatabase({
    synchronize: true,
    logging: false,
    entities: [
      `${__dirname}/../common/entity/Game.ts`,
      `${__dirname}/../common/entity/User.ts`,
    ],
    ...conf.database,
  })

  const helpers = initHelpers(entityManager)

  return {
    listen: listen(helpers),
  }
}

function listen(helpers: DBHelpers) {
  return (port: number) => {

    const wss = new WebSocket.Server({ port });

    (function movementUpdateTimeout() {
      setTimeout(async () => {
        await updatePositions(wss, helpers)
        movementUpdateTimeout()
      },         50)
    })()

    wss.on('connection', (ws: GameSocket) => {
      ws.on('message', async (message) => {
        const event = JSON.parse(message.toString())
        let updatedEvent: SocketEventResponse;

        switch (event.command) {
          case 'NEW_PLAYER':
            updatedEvent = await handleNewPlayer(helpers, event);
            ws.id = updatedEvent.playerId
            ws.gameId = updatedEvent.gameId
            broadcastEvent(wss, updatedEvent)
            break;
          case 'REFRESH_PLAYERS':
            updatedEvent = await handleRefreshPlayer(helpers, event);
            broadcastEvent(wss, updatedEvent)
            break;
          case 'MOVEMENT':
            await handleMovement(helpers, event)
            break;
        }
      });

    });
  }
}

async function updatePositions(wss: WebSocket.Server, helpers: DBHelpers) {
  const games = await helpers.game.get()

  for (const game of games) {
    const users = await helpers.user.getByGameId(game.id)

    for (const user of users) {
      if (user.positionX == null || user.positionY == null) {
        continue
      }

      if (user.updateSent) {
        continue;
      }

      const movementEvent: MovementResponse = {
        command: 'MOVEMENT',
        username: user.username,
        playerId: user.id,
        gameId: game.id,
        position: {
          x: user.positionX,
          y: user.positionY,
        },
      }

      broadcastEvent(wss, movementEvent)

      user.updateSent = true;
      helpers.user.put(user)
    }
  }
}

function broadcastEvent(wss: WebSocket.Server, event: SocketEventResponse) {
  const serializedEvent = JSON.stringify(event)

  console.log(event)

  const clients = wss.clients as Set<GameSocket>
  clients.forEach((client) => {
    if (client.gameId === event.gameId && client.readyState === WebSocket.OPEN) {
      client.send(serializedEvent);
    }
  });
}

export { makeServer }
