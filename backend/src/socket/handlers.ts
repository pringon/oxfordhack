import { DBHelpers } from '../common/entity'
import {
  NewPlayerRequest,
  NewPlayerResponse,
  RefreshPlayerRequest,
  RefreshPlayerResponse,
  MovementRequest,
  MovementResponse,
} from './events'

async function handleNewPlayer(
  helpers: DBHelpers,
  event: NewPlayerRequest,
): Promise<NewPlayerResponse> {

  if (event.gameId == null) {
    const game = helpers.game.make({});
    await helpers.game.put(game);
    event.gameId = game.id;
  }

  const player = helpers.user.make({
    gameId: event.gameId,
    username: event.username,
  });
  await helpers.user.put(player);

  const updatedEvent: NewPlayerResponse = {
    command: 'NEW_PLAYER',
    gameId: player.gameId,
    playerId: player.id,
    username: event.username,
    position: {
      x: player.positionX,
      y: player.positionY,
    },
  };

  return updatedEvent;
}

async function handleRefreshPlayer(
  helpers: DBHelpers,
  event: RefreshPlayerRequest,
): Promise<RefreshPlayerResponse> {
  console.log('Refresh existing player');

  const player = await helpers.user.getById(event.playerId);
  const updatedEvent: RefreshPlayerResponse = {
    command: 'REFRESH_PLAYERS',
    gameId: player.gameId,
    playerId: player.id,
    username: player.username,
    position: {
      x: player.positionX,
      y: player.positionY,
    },
  };

  return updatedEvent;
}

async function handleMovement(
  helpers: DBHelpers,
  event: MovementRequest,
): Promise<MovementResponse> {
  const user = await helpers.user.getById(event.playerId)

  user.positionX = event.position.x
  user.positionY = event.position.y
  user.updateSent = false;

  await helpers.user.put(user)

  const updatedEvent: MovementResponse = {
    ...event,
    username: user.username,
  }

  return updatedEvent
}

export {
  handleNewPlayer,
  handleRefreshPlayer,
  handleMovement,
}
