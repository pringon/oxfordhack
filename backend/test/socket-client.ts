import * as Websocket from 'ws';

const ws = new Websocket('ws://localhost:8080')

const state: {
    playerId: string
    gameId: string
    position: { x: number, y: number }
} = {
    playerId: null,
    gameId: null,
    position: {
        x: null,
        y: null
    }
}

ws.on('open', () => ws.send(
    Buffer.from(
        JSON.stringify({
            command: 'NEW_PLAYER',
            username: 'bob'
        })
    )
))

ws.on('message', (data) => {
    const { command, playerId, gameId, username, position} = JSON.parse(data.toString())
    switch (command) {
        case 'NEW_PLAYER':
            if (playerId === null) {
                state.playerId = playerId
                state.gameId = gameId
                state.position = position
            }
            if (playerId === state.playerId) {
                return
            }

            console.log(`Player ${playerId} has connected.`)
            console.log(`Game ${gameId}`)
            ws.send(
                Buffer.from(
                    JSON.stringify({
                        command: 'REFRESH_PLAYERS',
                        ...state
                    })
                )
            )
            break;
        case 'REFRESH_PLAYERS':
            if (playerId === state.playerId) {
                return
            }

            console.log(`Player ${playerId} is at pos: ${JSON.stringify(position)}`)
            break;
        case 'MOVEMENT':
            console.log(`Player ${username} is at ${JSON.stringify(position)}`)
    }
})